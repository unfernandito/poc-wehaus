/**
 * CameraController
 *
 * @description :: Server-side logic for managing cameras
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 const Path = require('path');
 const MjpegCamera = require('mjpeg-camera');
 const exec = require('child_process').exec;
 const FileOnWrite = require('file-on-write');
 const fs = require('fs');

module.exports = {
	saveVideo: (req, res) => {
		let date = new Date();
		let root = __dirname;
				root = root.substring(0, root.length - 15) + 'assets/';
		let route = 'images/frames/' + date.getDate() + date.getMonth() + date.getTime() + date.getSeconds() + '/';
		let folder = root+route;

		let fileWriter = new FileOnWrite({
				path: folder,
				ext: '.jpeg',
				filename: (frame) => {
						return frame.name + '-' + frame.time;
				},
				transform: (frame) => {
						return frame.data;
				}
		});

		console.log(req.param('hostname'));

		let camera = new MjpegCamera({
				name: 'wehaus',
				//user: 'admin',
				//password: 'wordup',
				url: req.param('hostname'),
				motion: false
		});

		camera.pipe(fileWriter);

		camera.start();

		setTimeout(function() {

				camera.stop();

				setTimeout(() => {
						exec('convert -delay 10 -quality 100 ' + folder + '*.jpeg ' + folder + 'video.mp4', (err, stdout, stderr) => {
								if (err) {
										console.log('exec error: ' + err);
										return;
								}
								console.log('stdout: ' + stdout);
								console.log('stderr: ' + stderr);
								return res.send({data: 'http://localhost:1337/'+route+"video.mp4"});
						})

				}, 10000 );

		}, parseInt(req.param('seconds')) * 1000);
	},

	saveScreenshot: (req, res) => {
		let date = new Date();
		let root = __dirname;
				root = root.substring(0, root.length - 15) + 'assets/';
		let route = 'images/frames/' + date.getDate() + date.getMonth() + date.getTime() + date.getSeconds() + '/';
		let folder = root+route;

		console.log(__dirname)

		let fileWriter = new FileOnWrite({
				path: folder,
				ext: '.jpeg',
				filename: (frame) => {
						return frame.name + '-' + frame.time;
				},
				transform: (frame) => {
						return frame.data;
				}
		});


		let camera = new MjpegCamera({
				name: 'wehaus',
				//user: 'admin',
				//password: 'wordup',
				url: req.param('hostname'),
				motion: true
		});

		camera.getScreenshot(function(err, frame) {
				if (err) {
						res.send({
								data: err
						})
				};
				let imageName = date + '-screenshot.jpeg';
				console.log('http://localhost:1337/'+route+imageName)
				fs.writeFile(folder + imageName, frame, (err, data) => {
					console.log("screenshot");
						return res.send({data: 'http://localhost:1337/'+route+imageName});
				});
		});
	}
};
