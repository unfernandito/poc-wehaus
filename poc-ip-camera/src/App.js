import React, { Component } from 'react';
import request from 'browser-request';
import logo from './logo.svg';
import './App.css';

let hostname = "";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      image: '',
      video: ''
    }
  }

  componentWillMount(){
    let hostnamePrompt = prompt("Coloca la camara a usar", hostname);
    hostname = hostnamePrompt;
  }

  saveVideo(){
    let seconds = document.getElementById('seconds').value

    request({method: 'POST', url: 'http://localhost:1337/camera/video', json: {
      hostname: hostname,
      seconds: seconds
    }}, (err, response, body) => {
      if(err){
        console.log(err)
      }

      document.getElementById('video-save').href = body.data;
      document.getElementById('video-save').click()
    });
    //http://190.210.40.43:8013/video/mjpg.cgi
  }

  saveImage(){
    console.log(request)
    request({method: 'POST', url: 'http://localhost:1337/camera/screenshot', json: {
      hostname: hostname
    }}, (err, response, body) => {
      if(err){
        console.log(err)
      }

      document.getElementById('img-save').href = body.data;
      document.getElementById('img-save').click()
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>POC Ip Camera</h2>
        </div>
        <p className="App-intro">
            <input type="text" disabled value={hostname} id="hostname" />
            <hr />
            <img src={hostname} alt="" />
            <br />
            <a href={this.state.image} hidden id="img-save" download="screenshot-image.jpeg" />
            <a src={this.state.video} hidden id="video-save" download="video.mp4" />
            <hr />
            <button onClick={this.saveImage}>Guardar imagen</button><br/>
            <label>Segundos</label><input id="seconds" type="number" step="1" min="20" max="60" /> <button onClick={this.saveVideo}>Guardar Video</button>
        </p>
      </div>
    );
  }
}

export default App;
