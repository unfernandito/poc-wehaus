import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'react-date-picker/index.css'
import { DateField, DatePicker } from 'react-date-picker'


let hostname = "";
//let socket = require('socket.io-client')('http://localhost:8000');
let images;

let pointImg;
let src;
let root;
//http://admin@190.210.40.43:8012/video/mjpg.cgi
const onChange = (dateString, { dateMoment, timestamp }) => {

  pointImg = dateString;
}

setInterval(() => {
  if(pointImg){
    let imageContainer = document.getElementById('videoStream');
    var srcDate = new Date(Date.parse(pointImg));
    pointImg = new Date(srcDate.getTime() + 1000);

    src = pointImg.getDate() + '-' + (pointImg.getMonth() + 1) + '-' + pointImg.getFullYear() + '_' + pointImg.getHours() + ':' + pointImg.getMinutes() + ':' + pointImg.getSeconds()
    console.log(src);

    root = pointImg.getDate() + '-' + (pointImg.getMonth() + 1) + '-' + pointImg.getFullYear()

    imageContainer.setAttribute('src', 'http://localhost:8000/images/frames/'+ root + '/wehaus_' + src + '.jpeg');
  }
}, 1000);

/*
setInterval(() => {
  pointImg = moment(pointImg.toString()).add(1, 'seconds').format('YYYY-MM-DD hh:mm:ss').toString()

  console.log(pointImg);
}, 5000);*/

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      images: [],
      folders: []
    }
  }

  componentWillMount(){
    let hostnamePrompt = prompt("Coloca la camara a usar", hostname);
    hostname = hostnamePrompt;
  }

  componentDidMount(){
    fetch("http://localhost:8000/folders")
    .then((data) =>{
      let folders = data.json()
      folders.then(data => {
        this.setState({ folders: data.folders });
        //console.log(data.folders)
      })
    });
    socket.on('data', (data) => {
      console.log(data);
      socket.emit('camera-connection', { hostname: hostname });
      /*socket.on('images', (data) => {
        let imageContainer = document.getElementById('videoStream');
        imageContainer.setAttribute('src', 'http://localhost:8000/images/frames/' + data.image + '.jpeg');
        imageContainer.setAttribute("height", "240");
        imageContainer.setAttribute("width", "320");
      });*/
    });
  }

  getImagesList(folder) {
    fetch("http://localhost:8000/folder/"+folder)
    .then((data) =>{
      images = data.json()
      images.then(data => {
        this.setState({ images: data.images });
        console.log(this.state.images)
      })
    });
  }

  listFolders() {
    let folders = this.state.folders;
    let listFoldersUl = document.getElementById('listFolders');
    return folders.forEach((folder) => {
      let linkContainer = document.createElement('li');
      linkContainer.setAttribute('class', 'list-group-item');
      linkContainer.textContent = folder;
      listFoldersUl.appendChild(linkContainer);
    });
  }

  setLive(){
    pointImg = null;
    let imageContainer = document.getElementById('videoStream');
    imageContainer.setAttribute('src', hostname);
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span className="sr-only">Toggle navigation</span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="#">PoC WeHaus</a>
                </div>
                <div id="navbar" className="navbar-collapse collapse">
                    <ul className="nav navbar-nav">
                        <li><a href="/">Main</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <div className="container">
        <br />
          <div className="App-intro">
            <div className="col-xs-12 col-md-6">
              <h2 className="text-center page-header">Video Container</h2>
              <img src={hostname} id="videoStream" className="img-responsive" alt="" />
              <hr />
              <button className="btn btn-lg btn-danger" onClick={this.setLive}>Ver Live</button>
              <hr />
              <input type="text" disabled value={hostname} id="hostname" className="form-control" />
            </div>

            <div className="col-xs-12 col-md-6">
              <div>
                <h2 className="page-header text-center">Carpetas Disponibles</h2>
                <ul id="listFolders" className="list-group">
                  {this.listFolders()}
                </ul>
              </div>

              <hr />

              <h2 className="text-center page-header">Seleccionar Fecha: </h2>

              <DateField
              dateFormat="YYYY-MM-DD HH:mm:ss"
              forceValidDate={true}
              defaultValue={1480088986786}
              onChange={onChange}
              >
                <DatePicker
                navigation={true}
                locale="en"
                forceValidDate={true}
                highlightWeekends={true}
                highlightToday={true}
                weekNumbers={true}
                weekStartDay={0}
                />
              </DateField>



            </div>


          </div>
        </div>
      </div>
    );
  }
}

export default App;
