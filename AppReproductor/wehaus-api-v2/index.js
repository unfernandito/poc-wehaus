const Path = require('path');
const MjpegCamera = require('mjpeg-camera');
const exec = require('child_process').exec;
const FileOnWrite = require('file-on-write');
const fs = require('fs');

let express = require('express');
let app = require('express')();
let serveIndex = require('serve-index')
let server = require('http').Server(app);
let io = require('socket.io')(server);
let Promise = require('bluebird');
let camera;

server.listen(8000);

app.use(express.static('public'));
/*app.use('/images', serveIndex('public/images/frames', {
    'icons': true
}))*/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/folder/:folder', (req, res) => {
  let images = [];
  fs.readdir('./public/images/frames/'+req.params.folder, (err, files) => {
    files.forEach((file) => {
      images.push(file);
    });
  })

  setTimeout(() => {
    res.send({images: images});
  }, 1000);
});

app.get('/folders', (req, res) => {
  let folders = [];
  fs.readdir('./public/images/frames/', (err, files) => {
    files.forEach((file) => {
      folders.push(file);
    });
  })

  setTimeout(() => {
    res.send({folders: folders});
  }, 1000);
});

io.on('connection', function(socket) {
    socket.emit('data', {
        data: "connected"
    });

    socket.on('camera-connection', function(data) {
        let date = new Date();
        let root = __dirname;
        console.log(__dirname)
            //      root = root.substring(0, root.length - 15) + 'assets/';
        let route = '/public/images/frames/';
        let dateRoot = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '/';
        let folder = root + route + dateRoot;

        let fileWriter = new FileOnWrite({
            path: folder,
            ext: '.jpeg',
            filename: (frame) => {
                let date = new Date();
                socket.emit('images', {
                    image: dateRoot + frame.name + '_' + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '_' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
                });
                return frame.name + '_' + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '_' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
            },
            transform: (frame) => {
                return frame.data;
            }
        });

        camera = new MjpegCamera({
            name: 'wehaus',
            //user: 'admin',
            //password: 'wordup',
            url: data.hostname,
            motion: false
        });

        camera.pipe(fileWriter);

        camera.start();
    });

    socket.on('disconnect', () => {
        camera.stop();
    });
});
