import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

let hostname = "";
let socket = require('socket.io-client')('http://localhost:8000');

class App extends Component {

  componentWillMount(){
    let hostnamePrompt = prompt("Coloca la camara a usar", hostname);
    hostname = hostnamePrompt;
  }

  componentDidMount(){
    socket.on('data', (data) => {
      console.log(data);
      socket.emit('camera-connection', { hostname: hostname });
      socket.on('images', (data) => {
        let imageContainer = document.getElementById('images');
        let imgElement = document.createElement('img');
        imgElement.setAttribute('src', 'http://localhost:8000/images/frames/' + data.image + '.jpeg');
        imgElement.setAttribute("height", "240");
        imgElement.setAttribute("width", "320");
        imageContainer.appendChild(imgElement);
      });
    });
  }

  showImages(){

    return true;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>POC Ip Camera Socket Recording</h2>
        </div>
        <p className="App-intro">
            <input type="text" disabled value={hostname} id="hostname" />
            <hr />
            <video src={hostname} alt=""></video>
            <hr />
            <button onClick={this.showImages}>Guardar Video</button>
            <br />
        </p>

        <div id="images">
        </div>

      </div>
    );
  }
}

export default App;
