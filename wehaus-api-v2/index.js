const Path = require('path');
const MjpegCamera = require('mjpeg-camera');
const exec = require('child_process').exec;
const FileOnWrite = require('file-on-write');
const fs = require('fs');

let express = require('express');
let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let camera;

server.listen(8000);

app.use(express.static('public'));

io.on('connection', function(socket) {
    socket.emit('data', {
        data: "connected"
    });

    socket.on('camera-connection', function(data) {
        let date = new Date();
        let root = __dirname;
        console.log( __dirname)
        //      root = root.substring(0, root.length - 15) + 'assets/';
        let route = '/public/images/frames/';
        let dateRoot  = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + '_' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '/';
        let folder = root + route + dateRoot;

        let fileWriter = new FileOnWrite({
            path: folder,
            ext: '.jpeg',
            filename: (frame) => {
                socket.emit('images', { image: dateRoot + frame.name + '-' + frame.time });
                return frame.name + '-' + frame.time;
            },
            transform: (frame) => {
                return frame.data;
            }
        });

        camera = new MjpegCamera({
            name: 'wehaus',
            //user: 'admin',
            //password: 'wordup',
            url: data.hostname,
            motion: false
        });

        camera.pipe(fileWriter);

        camera.start();
    });

    socket.on('disconnect', () => {
      camera.stop();
    });
});
